#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import sqlite3
import logging
from flask import Flask, jsonify, request, make_response, abort, send_from_directory
from simple_api.db import myDBObject

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger(__name__)
__version__ = "v1.0"
BASE_URI = "/api/"+__version__
LOGGER.info('Base URI :' + BASE_URI)
simple_api = Flask(__name__, static_folder='static')
_person_inmemory_dict = dict()


def check_phone_number(phone_number):
    """Function to check if the phone number is valid or not.
    Input :
        phone_number: A sting representing the phone number
    Output :
        true or False
    Action :
        check if string is in form of a 10 digit phone number with
        2 digit international code
    """
    assert isinstance(phone_number, basestring)
    LOGGER.debug('Phone Number: '+phone_number)
    # phone regex
    phone_regex = r'^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$'
    regex_match = re.match(phone_regex, phone_number)
    if regex_match:
        LOGGER.info('Phone Number matches regex')
        return True
    else:
        LOGGER.info('Phone Number does not match regex')
        return False


class Person(object):
    """
    A class to store the information about the person

    Attributes:
        first_name: A string representing the first name of Person
        last_name: A string representing the last name of Person
    """
    def __init__(self, first_name, last_name):
        """Returns a Person object whose first_name is *first_name* and
        last_name is *last_name*
        """
        assert isinstance(first_name, basestring)
        assert isinstance(last_name, basestring)
        self.first_name = first_name
        self.last_name = last_name

    def __eq__(self, other):
        return (other and
                self.first_name == other.first_name and
                self.last_name == other.last_name)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.first_name.lower()) ^ hash(self.last_name.lower())


def return_phone_dict(person_obj, phone_number):
    """
    returns a dict with Attributes first_name, last_name and phone_number
    Input:
        person_obj:  object of type Person
        phone_number: A string representing the phone number
    Output :
        a dictionary with first_name, last_name and phone_number Attributes
    """
    assert isinstance(person_obj, Person)
    assert check_phone_number(phone_number)

    new_obj = {'first_name': person_obj.first_name,
               'last_name': person_obj.last_name,
               'phone_number': phone_number}
    LOGGER.debug('Phone Dict: '+str(new_obj))
    return new_obj


def return_complete_phone_dict(person_dict):
    """
    returns a list of dict of persons and matching phone number
    Input:
        person_dict:  key=<class Person> value=phone_number
    Output :
        a list with dictionaries, with first_name, last_name
        and phone_number Attributes
    """
    return [return_phone_dict(each_person, person_dict[each_person])
            for each_person in person_dict]


@simple_api.route('/static/<path:path>')
def send_js(path):
    """
    Returns the static page at /static/path
    Example:
        GET /static/index.html
        will return index.html file from static folder
    """
    LOGGER.info("Serving Static file "+str(path))

    return send_from_directory(simple_api.static_folder, path)


@simple_api.errorhandler(404)
def not_found(error):
    """
    Returns a JSON with failed status and Error 404
    """
    LOGGER.error(str(error))
    return make_response(jsonify({'error': 'Not found',
                                  'status': 'failed'}), 404)


@simple_api.errorhandler(400)
def bad_request(error):
    """
    Returns a JSON with failed status and Error 404
    """
    LOGGER.error(str(error))
    return make_response(jsonify({'error': 'Bad request',
                                  'status': 'failed'}), 400)


@simple_api.errorhandler(500)
def internal_error_server(error):
    """
    Returns a JSON with failed status and Error 500
    """
    LOGGER.error(str(error))
    return make_response(jsonify({'error': 'Something Bad Happened',
                                  'status': 'failed'}), 500)


@simple_api.route(BASE_URI+"/phone")
def get_phone():
    """
    GET End point of the API
    returns all the phone number in SQL DB and in-memory object
    Input:
        None
         or
        HTTP Args : first_name and last_name
        example:
            GET /api/v1.0/phone?first_name=foo&last_name=bar
    Output:
        Returns a list of JSON object
        example of successful request:
            {
              "objects": [
                {
                  "first_name": "FOO",
                  "last_name": "BAR",
                  "phone_number": "232 232 2339"
                },
                {
                  "first_name": "Alpha",
                  "last_name": "BAR",
                  "phone_number": "232 232 2353"
                }
              ],
              "status": "success"
            }
        example of failed request:
            {
              "error": "<Error Type>",
              "status": "failed"
            }
    """
    LOGGER.info('Got GET request')
    LOGGER.debug('GET Args : '+str(request.args))

    #if no first_name nad last_name arguments
    #output all DB and inmemory data
    if (not request.args.get('first_name') and
            not request.args.get('last_name')):
        #generate dict of in-memory objects
        LOGGER.info('No HTTP Args, so dumping all SQLITE and in-memory')
        inmemory_list = return_complete_phone_dict(_person_inmemory_dict)
        sqlite_list = list()
        try:
            LOGGER.debug('Trying to get data from SQLITE')
            db_obj = myDBObject()
            db_obj.query("Select First_Name,Last_Name,Phone_Number from Phone")
            for each_elem in db_obj.fetch_all():
                sqlite_list.append({'first_name': each_elem[0],
                                    'last_name': each_elem[1],
                                    'phone_number': each_elem[2]})
        except sqlite3.Error as err:
            LOGGER.critical("Error %s:" % err.args[0])
            # print "Error %s:" % e.args[0]
            # sys.exit(1)
        #append sqlite and in-memory objects
        total_list = inmemory_list + sqlite_list
        #return JSON
        LOGGER.debug("total_list :"+ str(total_list))
        return jsonify({'objects': total_list,
                        'status': 'success'})
    # if only one is present (first_name or last_name)
    # BAD Request error
    if not request.args.get('first_name') or not request.args.get('last_name'):
        LOGGER.warning('Incomplete HTTP Args')
        abort(400)

    first_name = request.args.get('first_name')
    last_name = request.args.get('last_name')
    temp_person_instance = Person(first_name, last_name)
    LOGGER.info('Got HTTP Args')
    #check if in in-memory object
    if temp_person_instance in _person_inmemory_dict:
        LOGGER.info('Found in In-memory object')
        phone_number = _person_inmemory_dict[temp_person_instance]
        inmemory_list = return_phone_dict(temp_person_instance, phone_number)
        LOGGER.debug("inmemory_list :"+ str(inmemory_list))
        return jsonify({'objects': inmemory_list,
                        'status': 'success'}), 200
    else: #else check in SQLite
        LOGGER.info('Not found in in-memory object, checking in SQLite')
        sqlite_list = list()
        try:
            db_obj = myDBObject()
            db_obj.query("Select First_Name,Last_Name,Phone_Number \
                          from Phone WHERE First_Name=? and \
                          Last_Name=?", (first_name, last_name))
            fetchdb_output = db_obj.fetch_all()
            # print fetchdb_output
            if fetchdb_output is None or fetchdb_output == []:
                LOGGER.warning('Not found in SQLite also')
                abort(404)
            LOGGER.debug("FETCHALL OUTPUT"+ str(fetchdb_output))
            for each_elem in fetchdb_output:
                # print each_elem
                sqlite_list.append({'first_name': each_elem[0],
                                    'last_name': each_elem[1],
                                    'phone_number': each_elem[2]})

            # print sqlite_list
        except sqlite3.Error as err:
            LOGGER.critical("Error %s:" % err.args[0])
            abort(500)
            # print "Error %s:" % e.args[0]
            # sys.exit(1)
        LOGGER.debug("sqlite_list :"+ str(sqlite_list))
        return jsonify({'objects': sqlite_list,
                        'status': 'success'})


@simple_api.route(BASE_URI+"/phone", methods=['POST', 'PUT'])
def put_or_post_phone():
    """
    POST/PUT End point of the API
    POST:
        Adds the phone number to in-memory object
    PUT :
        Adds the phone number to sqlite object
        if same name is already available in sqlite, it overrides
        with new information
    Input:
        HTTP-Headers :
            Content-Type : application/json
        HTTP Body :
            A JSON Object Comprising of
            first_name, last_name and phone_number

        example:
            POST /api/v1.0/phone
            Content-Type : application/json

            {
              "first_name": "FOO",
              "last_name": "BAR",
              "phone_number": "232 232 2353"
            }
    Output:
        Returns the status
        example of successful request:
            {
              "status": "success"
            }
        example of failed request:
            {
              "error": "<Error Type>",
              "status": "failed"
            }
    """

    LOGGER.info('Got POST/PUT request')
    #check if json and if all keys are available
    if (not request.json or
            'first_name' not in request.json or
            'last_name' not in request.json or
            'phone_number' not in request.json):
        LOGGER.error('Not a JSON type Request or missing Content-Type Header')
        abort(400)

    first_name = request.json['first_name']
    last_name = request.json['last_name']
    phone_number = request.json['phone_number']
    LOGGER.debug('first_name : '+str(first_name))
    LOGGER.debug('last_name : '+str(last_name))
    LOGGER.debug('phone_number : '+str(phone_number))
    #check if phone number is valid
    if not check_phone_number(phone_number):
        LOGGER.error('Phone Number not complaint to Standard')
        abort(400)
    if request.method == 'POST':
        #creating a person type for storing Name
        temp_person_instance = Person(first_name, last_name)
        #check if in in-memory object
        if temp_person_instance not in _person_inmemory_dict:
            _person_inmemory_dict[temp_person_instance] = phone_number
        LOGGER.info('successful in adding to in-memory object')
        return jsonify({'status': 'success'}), 201
    elif request.method == 'PUT':
        try:
            db_obj = myDBObject()
            db_obj.query("INSERT INTO Phone VALUES(?, ?, ?)",
                         (first_name, last_name, phone_number))
            db_obj.commit()
            LOGGER.debug('db_obj insert commited')
        except sqlite3.Error as err:
            LOGGER.critical("Error %s:" % err.args[0])
            abort(500)
            # print "Error %s:" % e.args[0]
        LOGGER.info('successful in adding to in-memory object')
        return jsonify({'status': 'success'}), 201


