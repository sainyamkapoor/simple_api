#!/usr/bin/env python
# -*- coding: utf-8 -*-
from simple_api.db import myDBObject

db_obj = myDBObject()
#drop the old table if it exists
db_obj.query("DROP TABLE IF EXISTS Phone")
#create a new table with (first_name,last_name) as primary key
db_obj.query("CREATE TABLE Phone(First_Name TEXT, \
				Last_Name TEXT, Phone_Number TEXT, \
				PRIMARY KEY (First_Name, Last_Name))")
#Commit the DB to file storage
db_obj.commit()
