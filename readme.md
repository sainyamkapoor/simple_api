
Code a simple REST application using Flask that offers:

    a GET method that returns a phone number for a given name, or the whole list of names/phone numbers.
    a POST method to add a name and a phone number to an object in memory and in sqlite db
    a PUT method to write to an SQLite database table instead of the in memory object.
    a HTML page to consume data from GET request to the API

Directory Structure
.

    ├── init_db.py  -- to create the initial tables
    ├── MANIFEST
    ├── readme.md   -- this file
    ├── requirements.txt -- contains the pypi packages required for this package
    ├── setup.py  -- contains the setup information for this module
    ├── simple_api -- module folder
    │   ├── static -- module folder
    │   │   ├── index.html -- contains the html page to consume data from GET request 
    |   ├── api.py -- Contains the binds, URI and methods to control the flow of information
    │   ├── db.py -- Contains the DB object, which can be shared among multiple files
    │   ├── __init__.py
    └── test.py  -- Sample file to run the module


How to install:
    
    python setup.py install


How to run
    
    #!/usr/bin/env python
    from simple_api import api
    api.simple_api.run(host="0.0.0.0",port=8080,debug=True)


or simply use test.py

NOTE: Before running, run init_db.py to create the table structure.

API end-points : 

    
  URI : /api/v1.0/phone
    
  HTTP Methods:
    
    POST: 
        json in body with attributes first_name, last_name, & phone_number.
        "Content-Type : application/json" header is required for processing.
        stores in an in-memory object.

        example :
          Header:
            Content-Type : application/json
          Body:
            {
                'first_name': 'foo1',
                'last_name': 'bar1',
                'phone_number': '343-434-3434'
            }
    PUT: 
        json in body with attributes first_name, last_name, & phone_number
        "Content-Type : application/json" header is required for processing
        stores in a SQLite DB.

        example:
          Header:
              Content-Type : application/json
          Body:
              {
                  'first_name': 'foo1',
                  'last_name': 'bar1',
                  'phone_number': '343-434-3434'
              }
    GET:
        if no arguments return complete list of phone number in memory
        if arguments (first_name and last_name) given then it returns the specific object

        example:
          GET /api/v1.0/phone

            response:
                {
                  "objects": [
                    {
                      "first_name": "foo1",
                      "last_name": "bar1",
                      "phone_number": "232 232 2353"
                    },
                    {
                      "first_name": "foo2",
                      "last_name": "bar2",
                      "phone_number": "232 232 2353"
                    },
                    {
                      "first_name": "foo3",
                      "last_name": "bar3",
                      "phone_number": "232 232 2353"
                    }
                  ],
                  "status": "success"
                }

            GET /api/v1.0/phone?first_name=Foo3&last_name=bar3

              response:
                  {
                    "objects": {
                      "first_name": "foo3",
                      "last_name": "bar3",
                      "phone_number": "232 232 2353"
                    },
                    "status": "success"
                  }

Angular JS

        GET /static/index.html
              Makes a GET request and Displays the data into HTML Table


