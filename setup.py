from distutils.core import setup
from simple_api import api

setup(
    name='Simple_API',
    version=api.__version__,
    packages=['simple_api',],
    long_description=open('readme.md').read(),
    install_requires=[
        "Flask >= 0.10.1",

    ],
)